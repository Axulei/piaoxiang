<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="../css/bootstrap.css"/>
    <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <script>
        $(function () {
            $("#headsubmit").click(function () {
                $("form").submit();
            })
        })


    </script>

</head>

<body>
<div class="modal fade" id="modal">
    <!-- 自适应 -->
    <div class="modal-dialog modal-sm "
         style="margin-top: 100px; background-color: #F7F7F6;width: 360px;border-radius: 2px;">
        <!--边框与阴影 -->
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">更改头像</h4>
            </div>
            <div class="divider"></div>
            <div class="modal-body">
                <div>
                    <div style="width:160px;border-right: 1px solid gainsboro;float: left;padding-left: 20px;">
                        <label for="head">
                            <img id="fileimg" src="../img/onloadhead.png" height="120px"  width="120px" style="border-radius: 50%"/>
                        </label>
                        <form action="../file/uploadhead" method="post" enctype="multipart/form-data">
                            <input type="hidden" id="userid" name="userid" value="${user.id}">
                            <input id="head" type="file" name="file" style="display: none;"/>
                        </form>
                    </div>
                    <div style="width:160px;float: right;padding-left: 20px;">
                        <img src="${user.head}" width="100px" style="border-radius: 50%;"/>
                        <div style="text-align: center;width: 100px;">当前头像</div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="divider"></div>
            <div class="modal-footer" style="margin-top: 20px;">
                <div style="text-align: center;">
                    <button type="button" id="headsubmit"
                            style="width:70px;height:25px;border: none;background: gray;color: white;font-size: 12px;">
                        确认提交
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    var oFile = document.getElementById("head")
    var oView = document.getElementById("fileimg")
    oFile.addEventListener("change",function(){
        var fs = this.files[0]
        var reader = new FileReader()
        reader.onload = function(){
            oView.src = this.result;

        }


        reader.onprogress = function(){
            console.log("正在读取")
        }

        reader.readAsDataURL(fs)

    })
</script>
</body>

</html>