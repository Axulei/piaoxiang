<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript">
    function search() {
        var text = document.getElementById("sch").value;
        if (text == "" || text.replace(/\s*/g, "") == "") {
            window.location.reload();
        } else {
            window.location.href = "../index/searchPage.jsp?text=" + text;
        }
    }

    $(function () {

        var userId = "${user.id }"

        $("#shoppingcar").click(shopcar)

        $("#gopc").click(function () {
            var url = window.location.href;
            if (userId == null || userId == "") {
                if (url.indexOf("login") == -1) {
                    $("#loginmodal").modal("show");
                }
            } else {
                window.location.href = "/piaoxiang/pc/pc_top.jsp";
            }
        })


        $("#goor").click(function () {
            var url = window.location.href;
            if (userId == null || userId == "") {
                if (url.indexOf("login") == -1) {
                    $("#loginmodal").modal("show");
                }
            } else {
                window.location.href = "/piaoxiang/pc/pc_top.jsp?ifurl=orderlist";
            }
        })

    })

    function shopcar() {

        window.location.href = "/piaoxiang/index/bookShopping.jsp";

    }


</script>


<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

    <div class="container">
        <div class="navbar-header">
            <a href="../index/main.jsp" class="navbar-brand glyphicon glyphicon-home">首页</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav  navbar-right">
                <li>

                    <c:if test="${user != null }">
                        <a href="../pc/pc_top.jsp"> Hello：${user.name}</a>
                    </c:if>
                    <c:if test="${user == null }">
                        <a href="../login_register/login_register.jsp">
                            登录/注册
                        </a>
                    </c:if>

                </li>
                <li>
                    <a href="#" id="shoppingcar">购物车</a>
                </li>
                <li>
                    <a href="#" id="goor">我的订单</a>
                </li>
                <li>
                    <a href="#" id="gopc">个人中心
                    </a>

                </li>
                <li>
                    <a href="../book/queryByType?bookType=0&pageNum=1">全部图书</a>
                </li>
                <li>
                    <a href="#">服务</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="starter navbar-default">
    <div class="container">
        <div class="navbar-header col-md-4">
            <div class="navbar-brand tp">
                <img src="../img/LOGO/LOGO.png"/>
            </div>

        </div>
        <div class="col-md-6 col-md-offset-6">

            <div class="input-group col-md-8 sosuo">
                <input type="text" class="form-control" id="sch" placeholder="请输入查询内容"/>
                <span class="input-group-btn">
						<button class="btn btn-danger" onclick="search();">搜索</button>
						</span>
            </div>
            <div class="ss">
                <a href="../index/searchPage.jsp?text=诗词格律">诗词格律</a>
                <a href="../index/searchPage.jsp?text=园治">园治</a>
                <a href="../index/searchPage.jsp?text=目知录">目知录</a>
                <a href="../index/searchPage.jsp?text=沧浪诗话">沧浪诗话</a>
                <a href="../index/searchPage.jsp?text=唐长孺">唐长孺</a>
            </div>
        </div>
    </div>
</div>
<jsp:include page="../public/loginmodal.jsp"></jsp:include>