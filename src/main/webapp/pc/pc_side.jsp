<%@ page contentType="text/html;charset=UTF-8" language="java" %>

    <script>
        $(function () {
            $('#order_li').bind('click',jumporder);
            $('#detail_li').bind('click',jumpdetail);
            $('#index_li').bind('click',jumpindex);
            $('#collect_li').bind('click',jumpsc);
            $('#address_li').bind('click',jumpad);
            $('#password_li').bind('click',jumpup);


            function jumpindex() {
                $("iframe").attr("src","pc_index.jsp");
            }

            function jumporder() {
                $("iframe").attr("src","orderlist.jsp");
            }

            function jumpdetail() {
                $("iframe").attr("src","pc_detail.jsp");
            }

            function jumpsc() {
                $("iframe").attr("src","pc_sc.jsp");
            }

            function jumpad() {
                $("iframe").attr("src","pc_address.jsp");
            }

            function jumpup() {
                $("iframe").attr("src","pc_up.jsp");
            }


        })

    </script>

<div>
    <div class="container">
        <div class="row" style="width: 200px;text-align:center;">
            <div class="span2">
                <ul class="nav nav-pills  nav-stacked">
                    <li class="active" style="font-size: 20px;" id="index_li">
                        <a href="#">个人中心</a>
                    </li>
                    <li id="detail_li">
                        <a href="#">我的信息</a>
                    </li>
                    <li id="password_li">
                        <a href="#">修改密码</a>
                    </li>
                    <li id="collect_li">
                        <a href="#">我的收藏</a>
                    </li>
                    <li id="order_li">
                        <a href="#" >我的订单</a>
                    </li>
                    <li id="address_li">
                        <a href="#">收货地址</a>
                    </li>
                    <li id="exit">
                        <a href='javascript:if(confirm("是否退出")){
                            window.parent.location.href="../user/clear";
                        }'>退出登录</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>


</div>
