<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="../css/pc_address.css" />
    <style>
        .detail_update,.detail_delete{
            font-size: 10px;
            float: right;
            margin-right: 10px;
        }
        .receiving-border a{
            text-decoration: none;
        }
        .receiving-border a:hover{
            color: orangered;
            cursor:pointer;
        }
        #address_submit{
            float: right;
        }
        li{
            overflow: hidden;
            text-overflow:ellipsis;
            white-space:nowrap;
        }
    </style>
</head>

<body style="background-color: #F5F5F5">
<input type="hidden" id="userid" value="${user.id}">
<div class="address_nav"> 个人中心>收货地址</div>
<div class="receiving-border border">
    <div class="receiving-head">
        <h2 style="color: black; font-size:20px ;">收货地址</h2></div>
    <hr style="margin: 20px 0px;" />

    <div id="pcaddress" class="receiving-body">
        <div class="receiving-body-div border">
            <div class="receiving-body-center ">
                <a href="#" data-target="#modal" data-toggle="modal"><img src="../img/add.jpg" /></a>
                <span style="color: black;">添加新地址</span>
            </div>
        </div>

    </div>

    <div class="clear">

    </div>
</div>
<script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
<script>
    var userid = $("#userid").val();
    $.getJSON("../address/queryAddressById",{"userid":userid},function (data) {
        var str = "";

        $(data).each(function(){
            str += "<div class=\"receiving-body-div f-left border\">"+
                "<div class=\"receiving-detail\">"+
                "<a class=\"detail_delete\">删除<input type='hidden' value='"+this.id+"'></a>"+
                "<a class=\"detail_update\" href=\"#\" data-target=\"#modify\" data-toggle=\"modal\">修改<input type='hidden' value='"+this.id+"'></a>"+
                "<span class=\"name\"><h3 class=\"receiving-detail-name\">"+this.name+"</h3></span>"+
                "<div>"+
                "<ul>"+
                "<li><span class=\"phone\">"+this.phone+"</li></span>"+
                "<li><span class=\"address\">"+this.address+"</li></span>"+
                "<li><span class=\"postCode\">"+this.postCode+"</li></span>"+
                "</ul>"+

                "</div>"+
                "</div>"+
                "</div>";
        });
        ($("#pcaddress").eq(0)).append(str+"<div class='clearfix'></div>");
    })

    $(".receiving-border").on("click",".detail_delete",function () {

        var id = $(this).find("input").val();

        if (confirm("确认删除？")) {
            $.getJSON("../address/delAddressById",{"id": id},function (data) {
                if (data){
                    alert("删除成功！")
                    window.location.reload();
                }else {
                    alert("删除失败！")
                }
            })
        }

    })

    $(".receiving-border").on("click",".detail_update",function () {
        var id = $(this).find("input").val();
        $.getJSON("../address/queryAddressByAddressId", {"id": id}, function(data){
            var address1 = (data.address).split(" ")[0];
            var address2 = (data.address).split(" ")[1];
            console.log(address1);
            console.log(address2);
            $("input[name=address1]").val(address2);
            $("input[name=youZhen1]").val(data.postCode);
            $("input[name=username1]").val(data.name);
            $("input[name=phone1]").val(data.phone);
        });
        /* 修改地址 */
        $(".modifyaddress").click(function () {

            var reg = /^\d+$/;
            var reg1 = /^1[3456789]\d{9}$/;
            var address = $("#distpicker1 select option:selected").text()+ " " + ($(".modal-body input[name='address1']").val()).replace(/\s*/g,"");
            var code = $(".modal-body input[name='youZhen1']").val();
            var username = $(".modal-body input[name='username1']").val();
            var phone = $(".modal-body input[name='phone1']").val();
            if (code.length != 6 || !reg.test(code)){
                alert("请输入正确的邮编!")
            }else if (!reg1.test(phone)){
                alert("请输入正确的手机号")
            }
            else if ($(".modal-body input[name='address1']").val() == ""){
                alert("请输入详细地址!")
            }else if (username == "") {
                alert("请输入姓名!")
            }else {
                $.getJSON("../address/updateAddressById",{"id":id,"address":address,"postCode":code,"name":username,"phone":phone},function (data) {

                    if (data){
                        alert("修改地址成功！")
                        window.location.reload();
                    }else{
                        alert("修改地址失败!")
                    }
                })
            }
        })



    })

</script>
<jsp:include page="../public/addressmodal.jsp"></jsp:include>

</body>

</html>