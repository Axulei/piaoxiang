<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="icon" href="../img/favicon.ico" mce_href="../img/favicon.ico" type="image/x-icon">
    <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
    <script type="text/javascript">
        $(function(){
            /*查询所有图书分类*/
            $.getJSON("../book/queryBookType",{},function (data) {
                var str = "";
                $("select").empty();
                $(data).each(function () {
                    str+="<option value ='"+this.id+"' name='bookType'>"+this.type+"</option>"
                })
                $("select").append(str);

            })
            /*通过id查询对应的图书信息*/
            var id = getUrlParam("id");
            $.getJSON("../book/queryBookById", {"id": id}, function(data){
                $("input[name=id]").val(data.id);
                $("input[name=name]").val(data.name);
                $("input[name=picture]").val(data.picture);
                $("input[name=author]").val(data.author);
                $("input[name=publish]").val(data.publish);
                $("input[name=publishDate]").val(data.publishDate);
                $("input[name=price]").val(data.price);
                $("textarea").val(data.content);
                $("input[name=number]").val(data.number);
                $("option[value="+data.bookType+"]").attr("selected","selected");
            });

            /*提交进行更新*/
            $("input[type=button]:first").click(function(){
                $.getJSON("../book/updateBookById",$("form").serialize()+"&bookType="+$("option:selected").val(),function(data){
                    if(data){
                        alert("更新成功!");
                        window.location.href="admin.jsp";
                    }else{
                        alert("更新失败!");
                    }
                });
            })

        })
        function getUrlParam(key) {
            // 获取参数
            var url = window.location.search;
            // 正则筛选地址栏
            var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
            // 匹配目标参数
            var result = url.substr(1).match(reg);
            //返回参数值
            return result ? decodeURIComponent(result[2]) : null;
        }
    </script>
    <title>修改图书</title>
    <style type="text/css">
        #addBooks {
            width: 666px;
            border-collapse: collapse;
            margin: 0 auto;
        }

        #addBooks td {
            border: 1px solid #333333;
            padding: 3px 7px;
        }

        #addBooks tr:nth-child(1) {
            height: 50px;
            background-color: #337AB7;
            color: white;
            text-align: center;
            font-size: 3em;
        }

        span {
            color: red;
        }

        input {
            width: 200px;
        }

        button {
            height: 20px;
            width: 50px;
            border: 1px solid #cccccc;
            border-radius: 5px;
            margin-bottom: 5px;
        }
        textarea{
            width: 200px;
            height: 100px;
        }
    </style>
</head>
<body>
<form >
    <table id="addBooks">
        <tr>
            <td colspan="8" class="bookHead">
                修改图书
                <input type="hidden" name="id" value="">
            </td>
        </tr>
        <tr>
            <td>书名<span>(*)</span></td>
            <td><input type="text" name="name" id="name" value=""
                       required="required" /></td>
        </tr>
        <tr>
            <td>作者<span>(*)</span></td>
            <td><input type="text" name="author" id="author" value=""
                       required="required" /></td>
        </tr>
        <tr>
            <td>图片<span>(*)</span></td>
            <td><input type="text" name="picture" id="picture" value=""
                       required="required" /></td>
        </tr>
        <tr>
            <td>出版社<span>(*)</span></td>
            <td><input type="text" name="publish" id="publish" value=""
                       required="required" /></td>
        </tr>
        <tr>
            <td>出版时间<span>(*)</span></td>
            <td><input type="date" name="publishDate" id="publishDate"
                       value="" required="required" /></td>
        </tr>
        <tr>
            <td>价格</td>
            <td><input type="text" name="price" id="price" value=""/></td>
        </tr>
        <tr>
            <td>内容摘要</td>
            <td><textarea style="resize: none" name="content"></textarea></td>
        </tr>
        <tr>
            <td>数量<span>(*)</span></td>
            <td><input type="number" name="number" id="number" value=""/></td>
        </tr>
        <tr>
            <td>图书类型<span>(*)</span></td>
            <td>
                <select>

                </select>
            </td>
        </tr>
        <tr style="text-align: center;">
            <td colspan="2"><input type="button" value="提交">
                <input type="button"  onclick="window.history.back()" value="返回"></td>
        </tr>
    </table>
</form>
</body>
</html>
