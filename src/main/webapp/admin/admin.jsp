<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="icon" href="../img/favicon.ico" mce_href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/bootstrap.css" />
    <link rel="stylesheet" href="../css/admin_guanLi.css" />
    <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <script type="text/javascript" src="../js/admin_guanLi.js"></script>
    <title>飘香书屋后台管理系统</title>
    <style>
        .body_div{
            background: url("../img/admin/admin_background.jpg") no-repeat;
        }
    </style>
</head>
<body>
<div class="body_div">
    <!-- logo和标题部分 -->
    <div class="top">
        <img src="../img/LOGO/LOGO-white.png" style="height: 134px;width: 538px;" />
    </div>
    <div class="title">
    <span>当前登录用户：系统管理员&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span id="cg"></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

    </span>
        <a href="../user/clear">退出登录</a>
    </div>

    <!-- 主体部分 -->
    <div class="div1" style="background:#0F78B9;">
        <div class="main_left lf">
            <div class="main_left_top">
                <div class="yuan lf"></div>
                系统导航
                <div class="yuan lr"></div>
            </div>
            <div class="container-fluid">
                <div class="accordion" id="accordion2">
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <div class="accordion-toggle zhe_die" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                                图书管理
                            </div>
                        </div>
                        <div id="collapseOne" class="accordion-body collapse" style="height: 0px; ">
                            <div class="accordion-inner book">
                                <a href="#">展示图书信息</a><br />
                            </div>
                        </div>
                    </div>
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <div class="accordion-toggle zhe_die" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                                用户管理
                            </div>
                        </div>
                        <div id="collapseTwo" class="accordion-body collapse">
                            <div class="accordion-inner user">
                                <a href="#">展示用户信息</a><br />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="main_right lr">
            <!-- 图书展示页面 -->
            <div class="container bookdiv">
                <!-- 头部 -->
                <div class="row">
                    <div class="col-md-10" style="text-align: center"><h2>图书详细信息列表</h2></div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div class="input-group">
                            <input type="text" id="searchBook" placeholder="请输入关键词..." class="form-control input-lg">
                            <span onclick="searchBook(1,7)" class="input-group-addon btn btn-primary">搜索</span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <button onclick="addbook()" type="button" class="btn btn-primary" style="height:45px;width: 80px">
                            <span class="glyphicon glyphicon-plus adduser" aria-hidden="true"></span>增加
                        </button>
                    </div>
                </div>
                <!-- 身子 -->
                <div class="row">
                    <table class="table table-hover table-bordered alltable">
                        <thead>
                        <th >编号</th>
                        <th >书名</th>
                        <th >作者</th>
                        <th >出版社</th>
                        <th >价格</th>
                        <th >图书数量</th>
                        <th >图书类型</th>
                        <th >操作</th>
                        </thead>
                        <tbody class="bookTbody">
                        </tbody>
                    </table>
                </div>
                <!-- 分页 -->
                <div class="row text-center">
                    <nav aria-label="Page navigation">
                    </nav>
                </div>
            </div>
            <!-- 用戶展示頁面 -->
            <div class="container userdiv" style="display: none">
                <!-- 头部 -->
                <div class="row">
                    <div style="text-align: center"><h2>用户详细信息列表</h2></div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div class="input-group">
                            <input type="text" id="searchUser" placeholder="请输入关键词..." class="form-control input-lg">
                            <span onclick="searchUser(1,7)" class="input-group-addon btn btn-primary">搜索</span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <button onclick="adduser()" type="button" class="btn btn-primary" style="height:45px;width: 80px">
                            <span class="glyphicon glyphicon-plus adduser" aria-hidden="true"></span>增加
                        </button>
                    </div>
                </div>
                <!-- 身子 -->
                <div class="row">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <th>编号</th>
                        <th>用户名</th>
                        <th>密码</th>
                        <th>年龄</th>
                        <th style="width: 100px">性别</th>
                        <th>用户权限</th>
                        <th>手机号</th>
                        <th>生日</th>
                        <th >操作</th>
                        </thead>
                        <tbody class="UserTbody" id="userTbody">
                        </tbody>
                    </table>
                </div>
                <!-- 分页 -->
                <div class="row text-center">
                    <nav aria-label="Page navigation">
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
