package com.kgc.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookieUtil {

    public static void setCookie(String key, String val,int time,HttpServletResponse response) {
        Cookie userCookie = new Cookie(key,val);
        userCookie.setMaxAge(time * 24 * 60 * 60);
        userCookie.setPath("/");
        response.addCookie(userCookie);
    }

    public static String getCookie(String key,HttpServletRequest request) {

        String value = null;
        Cookie[] cookies = request.getCookies();
        if(cookies!= null && cookies.length>0) {
            for (Cookie cookie : cookies) {
                if(key.equals(cookie.getName())){
                    value = cookie.getValue();
                }
            }
        }
        return value;
    }


}