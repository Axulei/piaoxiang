package com.kgc.mapper;

import com.kgc.entity.ShippingAddress;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShippingAddressMapper {

    //查询用户下用没有收货地址
    int queryCountByUserId(@Param("userid")Integer userid);

    //查询用户下的所有收货地址
    List<ShippingAddress> queryAddressById(@Param("userid")Integer userid);

    //给用户增加一个收货地址
    int addAddress(@Param("address")ShippingAddress address);

    //通过addressid查询该地址
    ShippingAddress queryAddressByAddressId(@Param("id")Integer id);

    // 根据id删除地址
    int delAddressById(@Param("id") int id);

    // 根据id修改地址
    int updateAddressById(@Param("address") ShippingAddress address);
}
