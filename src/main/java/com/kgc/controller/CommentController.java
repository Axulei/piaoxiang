package com.kgc.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kgc.entity.Comment;
import com.kgc.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("comment")
@ResponseBody
public class CommentController {

    @Autowired
    private CommentService commentService;
    @RequestMapping("query")
    public List<Comment> queryByid(int bookid){
        return commentService.queryByid(bookid);
    }
    @RequestMapping("addcomment")
    public boolean add(Comment comment){
        return commentService.add(comment);
    }
    @RequestMapping("del")
    public boolean del(int id){return commentService.del(id);}
}
